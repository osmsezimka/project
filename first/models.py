from django.core.validators import RegexValidator
from django.db import models
from django.utils import timezone


class Client(models.Model):
    phoneNumberRegex = RegexValidator(regex=r"^\+?1?\d{8,15}$")
    phone_number = models.CharField(validators=[phoneNumberRegex],
                                    max_length=16,
                                    unique=True,
                                    verbose_name="Номер телефона")
    code = models.IntegerField(default=0, verbose_name='operator code')
    email = models.EmailField(max_length=200, null=True)
    t = timezone.now()


class Mailing(models.Model):

    d = models.DateTimeField(verbose_name='время рассылки')
    text = models.TextField(verbose_name='Текст сообщения для клиента')
    over_time = models.DateTimeField(verbose_name='Время окончания рассылки')



STATUS = (
    ('sent', 'отправлен'),
    ('no sent', 'не отправлен'),

)


class SMS(models.Model):
    d = models.DateTimeField(verbose_name='Время создания')
    status = models.CharField(max_length=100, choices=STATUS, verbose_name='статус отправки')
    clients = models.ForeignKey(Client, on_delete=models.CASCADE, related_name='client')
    mailing = models.ForeignKey(Mailing, on_delete=models.CASCADE, related_name='mailing')

    def save(self, *args, **kwargs):
        if self.mailing == self.clients:
            self.status = 'sent'
            super(SMS, self).save()
        else:
            self.status = 'no sent'
            super(SMS, self).save()
