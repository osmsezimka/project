from rest_framework import generics, viewsets

from .models import *
from .serializers import ClientSerializer, SMSSerializer, MailingSerializer


class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class SMSListView(generics.ListAPIView):
    queryset = SMS.objects.all()
    serializer_class = SMSSerializer


class MailingViewSet(viewsets.ModelViewSet):
    queryset = Mailing.objects.all()
    serializer_class = MailingSerializer


